Source: libshumate
Priority: optional
Maintainer: Debian GNOME Maintainers <pkg-gnome-maintainers@lists.alioth.debian.org>
Uploaders: Matthias Geiger <werdahias@debian.org>, Jeremy Bícha <jbicha@ubuntu.com>
Build-Depends: debhelper-compat (= 13),
               dh-sequence-gir,
               gi-docgen,
               gperf,
               libgirepository1.0-dev,
               gir1.2-freedesktop-dev,
               gir1.2-gdk-4.0-dev,
               gir1.2-gobject-2.0-dev,
               gir1.2-gsk-4.0-dev,
               gir1.2-gtk-4.0-dev,
               libcairo2-dev (>= 1.4),
               libglib2.0-dev (>= 2.74.0),
               libgtk-4-dev,
               libsoup-3.0-dev,
               libsqlite3-dev (>= 1.12.0),
               libsysprof-capture-4-dev,
               libjson-glib-dev (>= 1.6),
               libprotobuf-c-dev,
               meson (>= 0.55.0),
               valac (>= 0.11.0),
               xauth <!nocheck>,
               xvfb <!nocheck>
Standards-Version: 4.7.2
Section: libs
Homepage: https://gitlab.gnome.org/GNOME/libshumate
Vcs-Browser: https://salsa.debian.org/gnome-team/libshumate
Vcs-Git: https://salsa.debian.org/gnome-team/libshumate.git
Rules-Requires-Root: no

Package: libshumate-1.0-1
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends},
         ${shlibs:Depends},
         libshumate-common (>= ${source:Version})
Description: GTK4 widgets for embedded maps
 It supports numerous free map sources such as OpenStreetMap,
 OpenCycleMap, OpenAerialMap and Maps for free.

Package: libshumate-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: gir1.2-shumate-1.0 (= ${binary:Version}),
         libcairo2-dev (>= 1.4),
         libglib2.0-dev (>= 2.74.0),
         libgtk-4-dev,
         libshumate-1.0-1 (= ${binary:Version}),
         libsoup-3.0-dev,
         libsqlite3-dev (>= 1.12.0),
         libjson-glib-dev (>= 1.6),
         libprotobuf-c-dev,
         ${gir:Depends},
         ${misc:Depends}
Provides: ${gir:Provides}
Breaks: libshumate-1.0-1 (<< 1.0.1-2~)
Replaces: libshumate-1.0-1 (<< 1.0.1-2~)
Description: GTK4 widgets for embedded maps -- development files
 It supports numerous free map sources such as OpenStreetMap,
 OpenCycleMap, OpenAerialMap and Maps for free.
 .
 This package contains the base development headers.

Package: gir1.2-shumate-1.0
Architecture: any
Multi-Arch: same
Section: introspection
Depends: ${gir:Depends},
         ${misc:Depends}
Description: GTK4 widgets for embedded maps - gir bindings
 It supports numerous free map sources such as OpenStreetMap,
 OpenCycleMap, OpenAerialMap and Maps for free.
 .
 This package can be used by other packages using the GIRepository format to
 generate dynamic bindings.

Package: libshumate-common
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends},
         ${shlibs:Depends},
Description: GTK4 widgets for embedded maps - common files
 It supports numerous free map sources such as OpenStreetMap,
 OpenCycleMap, OpenAerialMap and Maps for free.
 .
 This package contains common data files and translations.

Package: libshumate-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: GTK4 widgets for embedded maps - documentation
 It supports numerous free map sources such as OpenStreetMap,
 OpenCycleMap, OpenAerialMap and Maps for free.
 .
 This package contains the API reference.
